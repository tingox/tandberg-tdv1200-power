# Testing

Currently the terminal doesn't work, it blows the fuse on the mains circuit (not on the power board in the terminal) when powered on.

Testing some components.

C20 - safety Y capacitor - RIFA - 4700 pF - measured in circuit with my cheap component tester - 3973 pF

C1  - safety X capacitor - 47 nF - measured in circuit with my cheap component tester - 44.2 nF + diode

CR1 - CR2 - BY127M - measured in circuit with my cheap component tester - reports as "2 diodes"

CR3 - CR4 - BY127M - measured in circuit with my cheap component tester - reports as diode

C2 - 220 uF - measured in circuit with my cheap component tester - 267.7 uF, esr= 0.18R, Vloss= 1%

C3 - 220 uF - measured in circuit with my cheap component tester - 253.6 uF, esr=0.21R, Vloss=0.9%
