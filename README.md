# Tandberg TDV 1200 terminal - power / deflection schematics

This repository contains pictures of the power / deflection PCB (printed circuit board), some annotated with component placement on the solder side.
It also contains my effort to recreate the schematic in KiCAD.

PCB size: 259 mm x 188m. The top and bottom corners on the right side (seen from component side) of the pcb is cut away in curve. There is a vertical noth on the top right corner 11.6 mm down and 2 mm from the right edge.
Orientation is defined by the "Tandberg Data" text on both sides of the pcb, when you can read the text normally the pcb is in the correct orientation.

Note: work in progress, not complete, might contain errors.

Solder side components:

Dpatch - diode BY127M with anode connected to R24 and R73, and cathode connected to C53, C62 and so on.

Rpatch - resistor connected across diode CR46.

Component notes:

T41 - eht / tube transformer

This is probably a standard cathode ray tube transformer, however I have not been able to find a datasheet.

Most of the solder pins are labeled on the solder side, so I have just numbered them for now. Pin 1 - label "FB", pin 2 - no label,
pin 3 - label "EM", pin 4 - label "C", pin 5 - label "HV", (oops, I skipped pin 6), pin 7 - label "H", pin 8 - label "H", pin 9 - label "55 V".

In addition, the EHT cable from the top of the transformer on the component side - this attaches to a hole in the tube.